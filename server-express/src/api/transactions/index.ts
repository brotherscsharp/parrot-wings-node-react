import * as controller from "./controller";
import { Router } from "express";
import { API } from "..";
import { authenticateToken } from "../../utils/jwtService";

export default class TransactionsApi implements API {
  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  setupApi() {
    this.router.get(
      "/transactions",
      authenticateToken,
      controller.getUsersTransactions
    );
    this.router.post("/transactions", authenticateToken, controller.create);
  }
}
