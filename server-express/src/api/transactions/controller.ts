import { Request, Response } from "express";
import { Errors } from "../../common";
import { getUserById } from "../users/service";
import { createTransaction, getUserTransactions } from "./service";

export let getUsersTransactions = async (req: Request, res: Response) => {
  // @ts-ignore
  const currentUserId = req.user ? req.user.id : "";
  const transactions = getUserTransactions(currentUserId);
  res.send(transactions);
};

export let create = async (req: Request, res: Response) => {
  // @ts-ignore
  const currentUserId = req.user ? req.user.id : "";
  const userSender = await getUserById(currentUserId);

  const receiverUserId = req.body.userid;
  const amount = req.body.amount;
  const userReceiver = await getUserById(receiverUserId);
  if (!userReceiver) {
    res.status(400).send({
      success: false,
      message: Errors.transactions.invalidUser,
    });
  }

  const balance = Number(userSender.balance);

  if (balance < amount) {
    res.status(400).send({
      success: false,
      message: Errors.transactions.balanceExceeded,
    });
  }

  const transaction = await createTransaction(userSender, userReceiver, amount);
  res.send(transaction);
};
