import { UserDocument } from "../../models/user.model";
import {
  Transaction,
  TransactionDocument,
} from "../../models/transaction.model";
import { updateUser } from "../users/service";
import logger from "../../utils/logger";

export const getUserTransactions = async (
  currentUserId: string
): Promise<TransactionDocument[]> =>
  await Transaction.find({
    historyUser: currentUserId,
  });

export const createTransaction = async (
  userSender: UserDocument,
  userReceiver: UserDocument,
  amount: number
): Promise<TransactionDocument> => {
  const balance = Number(userSender.balance);

  const session = await Transaction.db.startSession();

  session.startTransaction({
    readConcern: { level: "snapshot" },
    writeConcern: { w: "majority" },
  });

  try {
    // create transaction for receiver
    // @ts-ignore
    userReceiver.balance = userReceiver.balance + amount;
    await createTransactionModel(
      userReceiver,
      amount,
      userSender.name,
      userReceiver.name
    );
    // assert.ok(user.$session());
    // update user receiver
    updateUser(userReceiver);

    // create transaction for sender
    userSender.balance = balance - amount;
    const createdTransaction = await createTransactionModel(
      userSender,
      amount,
      userSender.name,
      userReceiver.name,
      true
    );

    // update user sender
    updateUser(userSender);
    await session.commitTransaction();

    return createdTransaction;
  } catch (error) {
    await session.abortTransaction();
    logger.info(`transaction incorrect. ${error}`);
  } finally {
    logger.info(`transaction successfully created`);
    session.endSession();
  }
};

const createTransactionModel = async (
  user: UserDocument,
  amount: number,
  userSenderName: string,
  userReceiverName: string,
  isSenderHistory: boolean = false
): Promise<TransactionDocument> => {
  const createdTransaction = new Transaction({
    balance: user.balance,
    userfrom: userSenderName,
    historyUser: user.id,
    username: userReceiverName,
    amount: amount,
  });
  if (isSenderHistory) {
    createdTransaction.amount = -createdTransaction.amount;
  }
  try {
    await createdTransaction.save();
    return createdTransaction;
  } catch {
    return undefined;
  }
};
