import { NextFunction, Request, Response } from "express";
import { Errors } from "../../common";
import { check, validationResult } from "express-validator";
import { createUser, getUser, getUserById, getUsers } from "./service";
import { generateAccessToken } from "../../utils/jwtService";

export const registerUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  await check("email", "Email is not valid").isEmail().run(req);
  await check("password", "Password is empty").not().isEmpty().run(req);
  await check("username", "User name is empty").not().isEmpty().run(req);

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    res.status(400).send({ message: Errors.user.badRequest });
  }

  const email = req.body.email || "";

  try {
    const user = await getUser({ email: email });
    if (!user) {
      const userModel = await createUser(
        req.body.username,
        req.body.email,
        req.body.password
      );
      if (userModel) {
        res.status(200).send({ id_token: generateAccessToken(userModel) });
      } else {
        res.status(400).send({
          success: false,
          message: Errors.user.cantCreateUser,
        });
      }
    } else {
      res.status(401).send({
        success: false,
        message: Errors.user.userExists,
      });
    }
  } catch (error) {
    res.status(401).send({
      success: false,
      message: error.message,
    });
  }
};

export const loginUser = async (
  req: Request,
  response: Response,
  next: NextFunction
) => {
  const email = req.body.email;
  const password = req.body.password;

  await check("email", "Email is not valid").isEmail().run(req);
  await check("password", "Password cannot be blank")
    .isLength({ min: 1 })
    .run(req);

  if (email && password) {
    try {
      const user = await getUser({ email: email });
      if (!user) {
        response.status(401).send({
          success: false,
          message: Errors.user.invalidEmailOrPassword,
        });
      } else {
        // check if password matches
        if (await user.comparePassword(password)) {
          response.status(200).send({ id_token: generateAccessToken(user) });
        } else {
          response.status(401).send({
            success: false,
            message: Errors.user.invalidEmailOrPassword,
          });
        }
      }
    } catch (error) {
      response.status(401).send({
        success: false,
        message: error.message,
      });
    }
  } else {
    return response.status(401).send({
      success: false,
      message: Errors.user.invalidEmailOrPassword,
    });
  }
};

export const filterUsers = async (req: Request, res: Response) => {
  try {
    const users = await getUsers(req.body.filter);
    res.status(200).send(users);
  } catch (error) {
    res.status(500).send({ message: "Oops! Can't get user info" });
  }
};

export const currentUserInfo = async (req: Request, res: Response) => {
  // @ts-ignore
  const currentUserId = req.user ? req.user.id : "";

  try {
    const user = await getUserById(currentUserId);
    res.status(200).send({
      id: user._id,
      name: user.name,
      email: user.email,
      balance: user.balance,
    });
  } catch (error) {
    res.status(500).send({ message: "Oops! Can't get user info" });
  }
};
