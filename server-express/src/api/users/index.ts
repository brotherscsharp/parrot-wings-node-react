import * as controller from "./controller";
import { Router } from "express";
import { API } from "..";
import { authenticateToken } from "../../utils/jwtService";

export default class UsersApi implements API {
  private router: Router;

  constructor(router: Router) {
    this.router = router;
  }

  setupApi() {
    this.router.post("/auth/register", controller.registerUser);
    this.router.post("/auth/login", controller.loginUser);
    this.router.get("/users", authenticateToken, controller.filterUsers);
    this.router.get(
      "/users/info",
      authenticateToken,
      controller.currentUserInfo
    );
  }
}
