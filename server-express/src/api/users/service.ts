import logger from "../../utils/logger";
import { User, UserDocument } from "../../models/user.model";
import { UserMeta } from "./dto/userMeta";

export const getUserById = async (id: string): Promise<UserDocument> => {
  return await User.findById(id);
};

export const getUser = async (filter: {}): Promise<UserDocument> => {
  return await User.findOne(filter);
};

export const getUsers = async (filter: string): Promise<UserMeta[]> => {
  const users = await User.find({});

  if (filter) {
    const filteredUsers = users
      .filter(
        (user: UserDocument) =>
          filter &&
          (user.name.toLowerCase().startsWith(filter.toLowerCase()) ||
            user.email.toLowerCase().startsWith(filter.toLowerCase()))
      )
      .map((user: UserDocument) => {
        return { id: user.id, name: user.name };
      });

    return filteredUsers;
  } else {
    const allUsers = users.map((user: UserDocument) => {
      return { id: user.id, name: user.name };
    });
    return allUsers;
  }
};

export const createUser = async (
  username: string,
  email: string,
  password: string
): Promise<UserDocument> => {
  const userModel = new User({
    name: username,
    email: email,
    password: password,
    balance: 500,
  });

  try {
    await userModel.save();
    logger.info(`User created`);
    return userModel;
  } catch (error) {
    logger.error(`Could not create user`);
    return undefined;
  }
};

export const updateUser = (user: UserDocument) => {
  User.findOneAndUpdate({ email: user.email }, user, {
    upsert: true,
  }).exec();
  logger.info(`User updated`);
};
