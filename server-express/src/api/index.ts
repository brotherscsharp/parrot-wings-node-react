import UsersApi from "./users";

import { Router, Express } from "express";
import TransactionsApi from "./transactions";

export let setupApis = (application: Express) => {
  const router = Router();
  const usersApi = new UsersApi(router);
  usersApi.setupApi();

  const transactionsApi = new TransactionsApi(router);
  transactionsApi.setupApi();

  application.use("/", router);
};

export interface API {
  setupApi(): any;
}
