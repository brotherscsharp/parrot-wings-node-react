import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import bluebird from "bluebird";
import compression from "compression";
import { MONGODB_URI } from "./utils/secrets";

import "./api";
import { setupApis } from "./api";
import passport, { session } from "passport";

const app = express();

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
mongoose.Promise = bluebird;

mongoose.set("useFindAndModify", false);
mongoose
  .connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch((err) => {
    console.log(
      `MongoDB connection error. Please make sure MongoDB is running. ${err}`
    );
    // process.exit();
  });

app.set("port", process.env.PORT || 3100);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

app.get("/", function (req, res) {
  res.send("Hello World!");
});

setupApis(app);

export default app;
