import mongoose from "mongoose";
import passportLocalMongoose from "passport-local-mongoose";
import { compare, genSalt, hash } from "bcryptjs";

export type UserDocument = mongoose.Document & {
  name: string;

  email: string;

  password: string;

  balance: Number;

  comparePassword: comparePasswordFunction;
};

const userSchema = new mongoose.Schema(
  {
    name: { type: String, unique: true },
    email: { type: String, unique: true },
    password: String,
    balance: Number,
  },
  { timestamps: true }
);

userSchema.pre("save", function save(next) {
  const user = this as UserDocument;
  if (!user.isModified("password")) {
    return next();
  }
  genSalt(10, async (err, salt) => {
    if (err) {
      return next(err);
    }
    try {
      const userHash = await hash(user.password, salt);
      user.password = userHash;
      next();
    } catch (error) {
      return next(err);
    }
  });
});

type comparePasswordFunction = (candidatePassword: string) => Promise<boolean>;

const comparePassword: comparePasswordFunction = async function (
  candidatePassword
): Promise<boolean> {
  try {
    return await compare(candidatePassword, this.password);
  } catch {
    return false;
  }
};

userSchema.methods.comparePassword = comparePassword;
userSchema.plugin(passportLocalMongoose);

export const User = mongoose.model<UserDocument>("User", userSchema);
