import mongoose from "mongoose";

export type TransactionDocument = mongoose.Document & {
  userfrom: String;
  username: String;
  historyUser: String;
  amount: Number;
  balance: Number;
};

const transactionSchema = new mongoose.Schema(
  {
    userfrom: { type: String },
    username: { type: String },
    historyUser: { type: String },
    balance: { type: Number },
    amount: { type: Number },
  },
  { timestamps: true }
);

export const Transaction = mongoose.model<TransactionDocument>(
  "Transaction",
  transactionSchema
);
