import { NextFunction, Request, Response } from "express";
import { UserDocument } from "../models/user.model";
import { Errors } from "../common";
import { SESSION_SECRET, TOKEN_TIME } from "./secrets";
const jwt = require("jsonwebtoken");

export function authenticateToken(
  req: Request,
  res: Response,
  next: NextFunction
) {
  // Gather the jwt access token from the request header
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (!token) return res.status(401).send({ message: Errors.user.badRequest });

  jwt.verify(token, SESSION_SECRET as string, (err: any, user: any) => {
    console.log(err);
    if (err)
      return res
        .status(403)
        .send({ message: Errors.messages.unauthorizedError });
    req.user = user;
    next(); // pass the execution off to whatever request the client intended
  });
}

export function generateAccessToken(user: UserDocument): string {
  return jwt.sign(
    { id: user.id, email: user.email, name: user.name },
    SESSION_SECRET,
    {
      expiresIn: `${TOKEN_TIME}s`,
    }
  );
}
