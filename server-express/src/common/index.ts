export const Errors = {
  messages: {
    unauthorizedError: "Unauthorized",
  },
  user: {
    badRequest: "You must send username and password",
    userExists: "A user with that email already exists",
    invalidEmailOrPassword: "Invalid email or password.",
    userNotFound: "user not found",
    noSearchString: "No search string",
    cantCreateUser: "Could not create user",
  },
  transactions: {
    invalidUser: "Invalid user",
    userNotFound: "user not found",
    balanceExceeded: "balance exceeded",
  },
};
