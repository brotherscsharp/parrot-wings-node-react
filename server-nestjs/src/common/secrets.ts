require("dotenv").config({ path: ".env" });

export const ENVIRONMENT = process.env.NODE_ENV;
const prod = ENVIRONMENT === "production"; // Anything else is treated as 'dev'

export const APP_PORT = process.env["PORT"];

export const MONGODB_URI = process.env["MONGODB_URI"];
export const JWT_SECRET = process.env["JWT_SECRET"];

export const USER_DEFAULT_BALANCE = process.env["USER_DEFAULT_BALANCE"];
