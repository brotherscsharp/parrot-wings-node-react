import { SchemaOptions } from "mongoose";
import { ApiPropertyOptional } from "@nestjs/swagger";
import { Typegoose, prop, pre } from "typegoose";
/**
 * Mongo's base Model and Schema
 */

@pre<BaseModel>("update", function (next) {
  this.updatedAt = new Date(Date.now());
  next();
})
export class BaseModel extends Typegoose {
  @prop()
  id: string;

  @prop({ default: Date.now() })
  createdAt?: Date;

  @prop({ default: Date.now() })
  updatedAt?: Date;
}

export const schemaOptions: SchemaOptions = {
  toJSON: {
    virtuals: true,
    getters: true,
  },
  timestamps: true,
}; /*
export class BaseModelVm {
  @ApiPropertyOptional({ type: String, format: "date-time" })
  createdAt?: Date;
  @ApiPropertyOptional({ type: String, format: "date-time" })
  updatedAt?: Date;
  @ApiPropertyOptional()
  id?: string;
}
*/

/**
 * View Models Base, will be exposed by API
 */
