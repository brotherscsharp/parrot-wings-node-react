export const Errors = {
  user: {
    badRequest: "You must send username and password",
    userExists: "A user with that email already exists",
    invalidEmailOrPassword: "Invalid email or password.",
    userNotFound: "user not found",
    noSearchString: "No search string",
    errorWhileCreateUser: "Error while creating user",
  },
  transactions: {
    unauthorizedError: "UnauthorizedError",
    invalidUser: "Invalid user",
    userNotFound: "user not found",
    balanceExceeded: "balance exceeded",
  },
};
