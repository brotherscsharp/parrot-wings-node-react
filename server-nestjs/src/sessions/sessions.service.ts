import { HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AuthService } from "../auth/auth.service";
import { Session } from "./sessions.model";
import { User } from "../users/users.model";

@Injectable()
export class SessionsService {
  constructor(
    @InjectModel("Session")
    private readonly sessionModel: Model<Session>,
    private authService: AuthService
  ) {}

  async create(user: User) {
    const token = this.authService.createJwtPayload(user);
    const createSession = new this.sessionModel({
      userId: user.id,
      hash: token.id_token,
      lastUpdate: new Date(),
    });
    await createSession.save();
    return token;
  }
}
