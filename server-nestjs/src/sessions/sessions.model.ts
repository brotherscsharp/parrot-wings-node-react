import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class Session {
  @Prop()
  userId: String;

  @Prop()
  hash: String;

  @Prop()
  lastUpdate: Date;
}

export const SessionSchema = SchemaFactory.createForClass(Session);
