import { Module } from "@nestjs/common";
import { SessionsService } from "./sessions.service";
import { MongooseModule } from "@nestjs/mongoose";
import { Session, SessionSchema } from "./sessions.model";
import { AuthModule } from "../auth/auth.module";
import { UsersService } from "../users/users.service";
import { User, UserSchema } from "../users/users.model";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Session.name, schema: SessionSchema },
      { name: User.name, schema: UserSchema },
    ]),
    AuthModule,
  ],
  providers: [SessionsService, UsersService],
  exports: [SessionsService],
})
export class SessionsModule {}
