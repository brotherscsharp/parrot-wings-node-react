import {
  Controller,
  Post,
  Get,
  Body,
  UseGuards,
  Request,
  HttpStatus,
  BadRequestException,
  UnauthorizedException,
} from "@nestjs/common";
import { ApiResponse } from "@nestjs/swagger";
import { CreateUserDto } from "./dto/create-user.dto";
import { UsersService } from "./users.service";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import { Errors } from "../common/error.strings";
import { InternalServerErrorException } from "@nestjs/common/exceptions/internal-server-error.exception";
import { UserToken } from "../auth/dto/user.token";
import { LoginUserDto } from "./dto/login-user.dto";
import { SessionsService } from "../sessions/sessions.service";
import { UserInfo, UserMeta } from "./users.model";

@Controller()
export class UsersController {
  constructor(
    private usersService: UsersService,
    private sessionService: SessionsService
  ) {}

  /**
   * Register user.
   * @route POST /auth/register
   */
  @Post("auth/register")
  @ApiResponse({ status: HttpStatus.CREATED, type: UserToken })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestException })
  async Add(@Body() user: CreateUserDto): Promise<UserToken> {
    const { email } = user;

    if (await this.usersService.findOne({ email })) {
      throw new BadRequestException(Errors.user.userExists);
    }

    try {
      return await this.usersService.create(user);
    } catch (error) {
      console.log(`${JSON.stringify(error)}`);
      throw new InternalServerErrorException(Errors.user.errorWhileCreateUser);
    }
  }

  /**
   * Login user and create session for him
   * @route POST /auth/login
   */
  @Post("auth/login")
  @ApiResponse({ status: HttpStatus.OK, type: UserToken })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestException })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedException })
  async signIn(@Body() user: LoginUserDto) {
    const { email } = user;

    const currentUser = await this.usersService.findOne({ email });

    if (!currentUser) {
      throw new BadRequestException(Errors.user.userNotFound);
    } else {
      // @ts-ignore
      if (currentUser.checkPassword(user.password)) {
        return this.sessionService.create(currentUser);
      } else {
        throw new UnauthorizedException(Errors.user.invalidEmailOrPassword);
      }
    }
  }

  /**
   * Filter users by name or email or get all users if filter does not set.
   * @route GET /users
   */
  @UseGuards(JwtAuthGuard)
  @Get("users")
  @ApiResponse({ status: HttpStatus.OK, type: UserMeta })
  async filterUsers(@Body() filter: any, @Request() request) {
    return await this.usersService.filteredUsers(
      request.user.id,
      filter.filter
    );
  }

  /**
   * Current user info.
   * @route GET /users/user-info
   */
  @UseGuards(JwtAuthGuard)
  @Get("users/info")
  @ApiResponse({ status: HttpStatus.OK, type: UserInfo })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestException })
  async getUserInfo(@Body() filter: String, @Request() request) {
    const user = await this.usersService.getCurrentUserInfo(request.user.id);

    if (!user) {
      throw new BadRequestException(Errors.user.userNotFound);
    }

    return {
      id: user.id,
      name: user.name,
      email: user.email,
      balance: user.balance,
    };
  }
}
