import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { PassportModule } from "@nestjs/passport";
import { SessionsService } from "src/sessions/sessions.service";
import { AuthModule } from "../auth/auth.module";
import { UsersController } from "./users.controller";
import { User, UserSchema } from "./users.model";
import { UsersService } from "./users.service";
import { Session, SessionSchema } from "../sessions/sessions.model";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Session.name, schema: SessionSchema },
    ]),
    PassportModule.register({ defaultStrategy: "jwt", session: false }),
    AuthModule,
  ],
  controllers: [UsersController],
  providers: [UsersService, SessionsService],
  exports: [UsersService],
})
export class UsersModule {}
