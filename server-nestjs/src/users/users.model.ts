import * as bcrypt from "bcrypt";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

export class UserMeta {
  id: string;
  name: string;
}

export class UserInfo extends UserMeta {
  email: string;
  balance: number;
}

@Schema()
export class User {
  @Prop()
  id: string;

  @Prop()
  name: string;

  @Prop()
  email: string;

  @Prop()
  password: string;

  @Prop()
  balance: number;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre("save", function (next) {
  let user = this;

  // Make sure not to rehash the password if it is already hashed
  if (!user.isModified("password")) return next();

  // Generate a salt and use it to hash the user's password
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.checkPassword = async function (attempt) {
  let user = this;
  try {
    return await bcrypt.compare(attempt, user.password);
  } catch {
    return false;
  }
};
