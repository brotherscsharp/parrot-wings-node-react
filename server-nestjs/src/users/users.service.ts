import { Model } from "mongoose";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { CreateUserDto } from "./dto/create-user.dto";
import { AuthService } from "../auth/auth.service";
import { User, UserMeta } from "./users.model";
import { JwtPayload } from "../auth/interfaces/jwt-payload.interface";
import { UnauthorizedException } from "@nestjs/common/exceptions/unauthorized.exception";
import { Guid } from "guid-typescript";
import { UserToken } from "../auth/dto/user.token";
import { USER_DEFAULT_BALANCE } from "../common/secrets";
import { compare, genSalt, hash } from "bcryptjs";
import { LoginUserDto } from "./dto/login-user.dto";

@Injectable()
export class UsersService {
  constructor(
    @InjectModel("User") private userModel: Model<User>,
    private authService: AuthService
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserToken> {
    const { email, password, username } = createUserDto;
    let createdUser = new this.userModel();

    createdUser.id = Guid.create();
    createdUser.balance = USER_DEFAULT_BALANCE;
    createdUser.name = username;
    createdUser.email = email;
    createdUser.password = await this.hashPassword(password);

    await createdUser.save();
    return this.authService.createJwtPayload(createdUser);
  }

  async getCurrentUserInfo(userId: string): Promise<User> {
    return await this.findOne({ id: userId });
  }

  async getAll(currentUserId: string): Promise<User[]> {
    const allUsers = await this.userModel.find({});
    return (
      allUsers ?? allUsers.filter((user: User) => user.id !== currentUserId)
    );
  }

  async findOne(filter = {}): Promise<User> {
    return this.userModel.findOne(filter).exec();
  }

  async validateUserByJwt(payload: JwtPayload) {
    const user = await this.findOne({ email: payload.email });

    if (user) {
      return this.authService.createJwtPayload(payload);
    } else {
      throw new UnauthorizedException();
    }
  }

  async filteredUsers(
    currentUserId: string,
    filterString: string = ""
  ): Promise<UserMeta[]> {
    const users = await this.getAll(currentUserId);

    return filterString
      ? users
          .filter(
            (user) =>
              filterString &&
              (user.name.toLowerCase().startsWith(filterString.toLowerCase()) ||
                user.email.toLowerCase().startsWith(filterString.toLowerCase()))
          )
          .map((user) => {
            return { id: user.id, name: user.name };
          })
      : users.map((user) => {
          return { id: user.id, name: user.name };
        });
  }

  protected async hashPassword(password: string): Promise<string> {
    const salt = await genSalt(12);
    const hashedPassword = await hash(password, salt);

    return hashedPassword;
  }

  updateUser(user: User) {
    this.userModel
      .findOneAndUpdate({ email: user.email }, user, {
        upsert: true,
      })
      .exec();
  }
}
