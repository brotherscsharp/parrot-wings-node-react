import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { JwtPayload } from "./interfaces/jwt-payload.interface";
import { UserToken } from "./dto/user.token";

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  createJwtPayload(user: any): UserToken {
    const data: JwtPayload = {
      email: user.email,
      id: user.id,
    };

    return {
      id_token: this.jwtService.sign(data),
    };
  }
}
