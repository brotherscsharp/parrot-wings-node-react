import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./strategies/jwt.strategy";
import { PassportModule } from "@nestjs/passport";
import { UsersService } from "../users/users.service";
import { MongooseModule } from "@nestjs/mongoose";
import { User, UserSchema } from "../users/users.model";
import { JWT_SECRET } from "../common/secrets";

const passportModule = PassportModule.register({
  defaultStrategy: "jwt",
});
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    passportModule,
    JwtModule.register({
      secret: JWT_SECRET,
      signOptions: {
        expiresIn: 36000,
      },
    }),
  ],
  exports: [AuthService, passportModule],
  providers: [AuthService, JwtStrategy, UsersService],
})
export class AuthModule {}
