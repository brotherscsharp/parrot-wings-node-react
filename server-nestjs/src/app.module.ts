import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UsersModule } from "./users/users.module";
import { AuthModule } from "./auth/auth.module";
import { SessionsModule } from "./sessions/sessions.module";
import { LoggerService } from "./logger/logger.service";
import { MONGODB_URI } from "./common/secrets";
import { TransactionsModule } from "./transactions/transactions.module";

@Module({
  imports: [
    MongooseModule.forRoot(MONGODB_URI, {
      useFindAndModify: false,
    }),
    UsersModule,
    AuthModule,
    SessionsModule,
    TransactionsModule,
  ],
  providers: [LoggerService],
})
export class AppModule {}
