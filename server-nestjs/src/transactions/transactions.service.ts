import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Transaction } from "./transaction.model";
import { UsersService } from "../users/users.service";
import { User } from "../users/users.model";
import { CreateTransactionDto } from "./dto/create-transaction.dto";

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel("Transaction") private transactionModel: Model<Transaction>,
    @InjectModel("User") private userModel: Model<User>,
    private usersService: UsersService
  ) {}

  async getUserTransactions(currentUserId: string): Promise<Transaction[]> {
    return await this.transactionModel.find({
      historyUser: currentUserId,
    });
  }

  async create(
    userSender: User,
    userReceiver: User,
    transaction: CreateTransactionDto
  ): Promise<Transaction> {
    const balance = Number(userSender.balance);

    const session = await this.transactionModel.db.startSession();

    session.startTransaction({
      readConcern: { level: "snapshot" },
      writeConcern: { w: "majority" },
    });

    try {
      // create transaction for receiver
      userReceiver.balance = userReceiver.balance + transaction.amount;
      await this.createTransaction(
        userReceiver,
        transaction,
        userSender.name,
        userReceiver.name
      );

      // update user receiver
      this.usersService.updateUser(userReceiver);

      // create transaction for sender
      userSender.balance = balance - transaction.amount;
      const createdTransaction = await this.createTransaction(
        userSender,
        transaction,
        userSender.name,
        userReceiver.name,
        true
      );

      // update user sender
      this.usersService.updateUser(userSender);

      return createdTransaction;
    } catch (error) {
      await session.abortTransaction();
    } finally {
      session.endSession();
    }
  }

  async createTransaction(
    user: User,
    transaction: CreateTransactionDto,
    userSenderName: string,
    userReseiverName: string,
    isSenderHistory: boolean = false
  ): Promise<Transaction> {
    let createdTransaction = new this.transactionModel(transaction);
    try {
      createdTransaction.date = new Date();
      createdTransaction.balance = user.balance;
      createdTransaction.username = userReseiverName;
      createdTransaction.userfrom = userSenderName;
      createdTransaction.historyUser = user.id;
      if (isSenderHistory) {
        createdTransaction.amount = -createdTransaction.amount;
      }

      await createdTransaction.save();
      return createdTransaction;
    } catch {
      throw new InternalServerErrorException();
    }
  }
}
