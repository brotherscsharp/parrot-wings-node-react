import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "../auth/auth.module";
import { User, UserSchema } from "../users/users.model";
import { UsersService } from "../users/users.service";
import { Transaction, TransactionSchema } from "./transaction.model";
import { TransactionsController } from "./transactions.controller";
import { TransactionsService } from "./transactions.service";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Transaction.name, schema: TransactionSchema },
      { name: User.name, schema: UserSchema },
    ]),
    AuthModule,
  ],
  controllers: [TransactionsController],
  providers: [TransactionsService, UsersService],
  exports: [TransactionsService],
})
export class TransactionsModule {}
