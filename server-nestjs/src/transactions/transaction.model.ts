import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class Transaction {
  @Prop()
  id: string;

  @Prop()
  userfrom: String;

  @Prop()
  username: String;

  @Prop()
  historyUser: String;

  @Prop()
  amount: Number;

  @Prop()
  balance: Number;

  @Prop()
  date: Date;
}
export const TransactionSchema = SchemaFactory.createForClass(Transaction);
