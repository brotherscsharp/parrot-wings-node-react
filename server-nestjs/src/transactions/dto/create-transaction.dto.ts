import { IsNotEmpty } from "class-validator";

export class CreateTransactionDto {
  @IsNotEmpty()
  readonly userid: string;
  @IsNotEmpty()
  readonly amount: number;
}
