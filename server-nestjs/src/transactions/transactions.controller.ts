import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
  HttpStatus,
  Request,
  BadRequestException,
  UnauthorizedException,
} from "@nestjs/common";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import { CreateTransactionDto } from "./dto/create-transaction.dto";
import { TransactionsService } from "./transactions.service";
import { ApiResponse } from "@nestjs/swagger";
import { Transaction } from "./transaction.model";
import { UsersService } from "../users/users.service";
import { Errors } from "../common/error.strings";

@Controller()
export class TransactionsController {
  constructor(
    private transactionsService: TransactionsService,
    private usersService: UsersService
  ) {}

  /**
   * Send fund to user
   * @route POST /transactions
   */
  @UseGuards(JwtAuthGuard)
  @Post("transactions")
  @ApiResponse({ status: HttpStatus.OK, type: Transaction })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: BadRequestException })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedException })
  async addTransaction(
    @Body() transaction: CreateTransactionDto,
    @Request() request
  ) {
    const userSender = await this.usersService.findOne({ id: request.user.id });
    if (!userSender) {
      throw new UnauthorizedException(Errors.transactions.unauthorizedError);
    }

    const userReceiver = await this.usersService.findOne({
      id: transaction.userid,
    });
    if (!userReceiver) {
      throw new BadRequestException(Errors.transactions.invalidUser);
    }

    const balance = Number(userSender.balance);

    if (balance < transaction.amount) {
      throw new BadRequestException(Errors.transactions.balanceExceeded);
    }

    return await this.transactionsService.create(
      userSender,
      userReceiver,
      transaction
    );
  }

  /**
   * Get all user's transactions
   * @route GET /transactions
   */
  @UseGuards(JwtAuthGuard)
  @Get("transactions")
  @ApiResponse({ status: HttpStatus.OK, type: Transaction })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedException })
  async getTransactions(@Body() filter: String, @Request() request) {
    const user = await this.usersService.getCurrentUserInfo(request.user.id);
    if (!user) {
      throw new UnauthorizedException(Errors.transactions.unauthorizedError);
    }

    return await this.transactionsService.getUserTransactions(user.id);
  }
}
