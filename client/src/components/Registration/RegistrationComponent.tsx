import React, { useEffect } from "react";
import { Input, Button, message, Layout } from "antd";
import "./RegistrationComponent.css";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import Strings from "../../constants";
import {
  backPwToInitialState,
  doRegister,
} from "../../reducers/ParrotWingsReducer";

import { Formik, Form, FormikHelpers } from "formik";
import * as Yup from "yup";
import { AuthService } from "../../networking/AuthService";

const { Header, Content, Footer } = Layout;
type FormValues = {
  email: string;
  password: string;
  repassword: string;
  userName: string;
};

const SignUpSchema = Yup.object().shape({
  password: Yup.string()
    .min(2, "Too short password")
    .max(20, "Too long password")
    .required("Required"),
  repassword: Yup.string()
    .required("Required")
    .oneOf([Yup.ref("password")], "Passwords are not equal!"),
  userName: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

type Props = {
  updateState: (authorized: boolean) => void;
};
export const RegistrationComponent = ({ updateState }: Props) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const error = useSelector((state: any) => state.pw.error);

  useEffect(() => {
    if (error) {
      message.error(error.error);
      dispatch(backPwToInitialState());
    }
  }, [dispatch, error, history]);

  const onSubmit = async (
    values: FormValues,
    { resetForm }: FormikHelpers<FormValues>
  ) => {
    try {
      const { email, password, userName } = values;
      await dispatch(doRegister({ email, password, username: userName }));
      resetForm({});
      updateState(AuthService.isUserLoggedIn());
      history.push("/");
    } catch (err) {
      message.error(error.message);
    }
  };
  return (
    <Layout className="layout">
      <Header>
        <p className="headerText">{Strings.appTexts.appName}</p>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="regisctrationContainer">
          <h2>{Strings.appTexts.registerTitle}</h2>
          <Formik
            initialValues={{
              password: "",
              repassword: "",
              userName: "",
              email: "",
            }}
            validationSchema={SignUpSchema}
            validateOnBlur={false}
            validateOnChange={false}
            onSubmit={onSubmit}
          >
            {({ handleChange, handleSubmit, values, errors, touched }) => (
              <Form>
                <div style={{ marginBottom: "20px" }}>
                  <Input
                    name="userName"
                    value={values.userName}
                    placeholder="User name"
                    onChange={handleChange("userName")}
                  />
                  {errors.userName && touched.userName ? (
                    <div className="errorText">{errors.userName}</div>
                  ) : null}
                </div>

                <div style={{ marginBottom: "20px" }}>
                  <Input
                    name="email"
                    type="email"
                    value={values.email}
                    placeholder="E-mail"
                    onChange={handleChange("email")}
                  />
                  {errors.email && touched.email ? (
                    <div className="errorText">{errors.email}</div>
                  ) : null}
                </div>

                <div className="formItemContainer">
                  <Input.Password
                    name="password"
                    value={values.password}
                    placeholder="Password"
                    onChange={handleChange("password")}
                  />
                  {errors.password ? (
                    <div className="errorText">{errors.password}</div>
                  ) : null}
                </div>

                <div className="formItemContainer">
                  <Input.Password
                    name="repassword"
                    value={values.repassword}
                    placeholder="Repeat password"
                    onChange={handleChange("repassword")}
                  />
                  {errors.repassword ? (
                    <div className="errorText">{errors.repassword}</div>
                  ) : null}
                </div>

                <Button
                  type="primary"
                  htmlType="submit"
                  className="submit"
                  onSubmit={() => handleSubmit()}
                >
                  Sign up
                </Button>

                <Link to="/signin" className="link">
                  have an account?
                </Link>
              </Form>
            )}
          </Formik>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        {Strings.appTexts.copyright}
      </Footer>
    </Layout>
  );
};
