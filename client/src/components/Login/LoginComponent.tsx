import React, { useEffect } from "react";
import { Input, Button, message, Layout } from "antd";
import "./LoginComponent.css";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import Strings from "../../constants";
import {
  backPwToInitialState,
  doLogin,
} from "../../reducers/ParrotWingsReducer";

import { Formik, Form, FormikHelpers } from "formik";
import * as Yup from "yup";
import { AuthService } from "../../networking/AuthService";

const { Header, Content, Footer } = Layout;
type FormValues = {
  email: string;
  password: string;
};

const SignInSchema = Yup.object().shape({
  password: Yup.string()
    .min(2, "Too short password")
    .max(20, "Too long password")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

type Props = {
  updateState: (authorized: boolean) => void;
};

export const LoginComponent = ({ updateState }: Props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const error = useSelector((state: any) => state.pw.error);

  useEffect(() => {
    if (error) {
      message.error(error);
      dispatch(backPwToInitialState());
    }
  }, [dispatch, error]);

  const onSubmit = async (
    values: FormValues,
    { resetForm }: FormikHelpers<FormValues>
  ) => {
    try {
      await dispatch(
        doLogin({ email: values.email, password: values.password })
      );
      updateState(AuthService.isUserLoggedIn());
      resetForm();
      history.push("/");
    } catch (err) {
      message.error(error.message);
    }
  };
  return (
    <Layout className="layout">
      <Header>
        <p className="headerText">{Strings.appTexts.appName}</p>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="loginContainer">
          <h2>{Strings.appTexts.logInTitle}</h2>
          <Formik
            initialValues={{
              password: "",
              email: "",
            }}
            validationSchema={SignInSchema}
            validateOnBlur={false}
            validateOnChange={false}
            onSubmit={onSubmit}
          >
            {({ handleChange, handleSubmit, values, errors, touched }) => (
              <Form>
                <div style={{ marginBottom: "20px" }}>
                  <Input
                    name="email"
                    type="email"
                    value={values.email}
                    placeholder="Type your e-mail here"
                    onChange={handleChange("email")}
                  />
                  {errors.email && touched.email ? (
                    <div className="errorText">{errors.email}</div>
                  ) : null}
                </div>

                <div className="formItemContainer">
                  <Input.Password
                    name="password"
                    value={values.password}
                    placeholder="Type your password here"
                    onChange={handleChange("password")}
                  />
                  {errors.password ? (
                    <div className="errorText">{errors.password}</div>
                  ) : null}
                </div>

                <Button
                  type="primary"
                  htmlType="submit"
                  className="submit"
                  onSubmit={() => handleSubmit()}
                >
                  Sign in
                </Button>

                <Link to="/registration" className="link">
                  first time here?
                </Link>
              </Form>
            )}
          </Formik>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        {Strings.appTexts.copyright}
      </Footer>
    </Layout>
  );
};
