import React from "react";
import { Avatar, Col, Row } from "antd";
import { UserOutlined } from "@ant-design/icons";
import "./UserDetailsComponent.css";
import Strings from "../../constants";
import { IUserInfo } from "../../protocols/IUserInfo";

type Props = {
  userInfo: IUserInfo;
};

export const UserDetailsComponent = ({ userInfo }: Props) => {
  return (
    <Row justify="end" align="top">
      <Col span={4}>
        <Avatar size={48} icon={<UserOutlined />} />
      </Col>
      <Col span={20}>
        <div className="userNameWrapper">
          <p className="userName">{userInfo.name}</p>
          <p className="userEmail">
            {Strings.appTexts.balance} <b>{userInfo.balance}</b>
          </p>
        </div>
      </Col>
    </Row>
  );
};
