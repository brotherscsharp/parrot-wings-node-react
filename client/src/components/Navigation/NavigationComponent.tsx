import React, { useEffect, useState } from "react";
import { Route, BrowserRouter, Switch, Redirect } from "react-router-dom";
import { configureStore } from "@reduxjs/toolkit";
import { LoginComponent } from "../Login/LoginComponent";
import { RegistrationComponent } from "../Registration/RegistrationComponent";
import { MainComponent } from "../Main/MainComponent";
import reducers from "../../reducers";
import { SendMoneyComponent } from "../SendMoney/SendMoneyComponent";
import { AuthService } from "../../networking/AuthService";

const store = configureStore({
  reducer: reducers,
});
export type AppDispatch = typeof store.dispatch;

function NavigationComponent() {
  const [isAuthorized, setIsAuthorized] = useState<boolean>(false);

  useEffect(() => {
    if (!isAuthorized) {
      setIsAuthorized(AuthService.isUserLoggedIn());
    }
  }, [isAuthorized]);

  return (
    <div className="App">
      <BrowserRouter>
        <div>
          <Switch>
            <Route exact path="/signin">
              <LoginComponent updateState={(value) => setIsAuthorized(value)} />
            </Route>
            <Route path="/registration">
              <RegistrationComponent
                updateState={(value) => setIsAuthorized(value)}
              />
            </Route>

            <ProtectedRoute
              isAllowed={isAuthorized}
              exact
              path="/sendmoney"
              component={SendMoneyComponent}
            />

            <ProtectedRoute
              isAllowed={isAuthorized}
              exact
              path="/"
              component={MainComponent}
            />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

// @ts-ignore
const ProtectedRoute = ({ isAllowed, ...props }) =>
  isAllowed ? <Route {...props} /> : <Redirect to="/signin" />;

export default NavigationComponent;
