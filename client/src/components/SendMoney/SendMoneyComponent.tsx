import React, { useEffect, useState } from "react";
import {
  Form,
  Button,
  message,
  InputNumber,
  AutoComplete,
  Row,
  Col,
  Layout,
} from "antd";
import "./SendMoneyComponent.css";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { getUsersList, sendFunds } from "../../reducers/SendFundsReducer";
import Strings from "../../constants";
import { UserDetailsComponent } from "../UserDetails/UserDetailsComponent";
import { doLogout } from "../../reducers/ParrotWingsReducer";
import { IUser } from "../../protocols/IUser";

const { Header, Content, Footer } = Layout;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export const SendMoneyComponent = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const [usersList, setUsersList] = useState([]);
  const responseMessage = useSelector((state: any) => state.funds.message);
  const filteredUsers = useSelector((state: any) => state.funds.filteredUsers);
  const userInfo = useSelector((state: any) => state.pw.userInfo);

  useEffect(() => {
    if (responseMessage) {
      if (responseMessage === "success") {
        message.success(Strings.messages.transactionSuccess);
      } else {
        message.error(responseMessage);
      }
    }

    if (!filteredUsers || filteredUsers.length === 0) {
      // @ts-ignore
      dispatch(getUsersList({}));
    } else {
      setUsersList(
        filteredUsers.map((user: IUser) => ({
          value: user.name,
          label: user.name,
        }))
      );
    }
  }, [dispatch, filteredUsers, responseMessage]);

  const onFinish = (values: any) => {
    console.log("Validation Success:", values);
    const selectedUser = filteredUsers.find(
      (user: IUser) => user.name === values.userNameField
    );
    if (selectedUser) {
      dispatch(sendFunds({ id: selectedUser.id, amount: values.amountField }));
    } else {
      message.error(Strings.messages.userNotFound);
    }
  };

  const filterUsers = (inputValue: string, option: any) => {
    if (option.value)
      return (
        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
      );
    else return false;
  };

  return (
    <Layout className="layout">
      <Header>
        <p className="headerText">{Strings.appTexts.appName}</p>

        <Row>
          <Col span={16}>
            <div style={{ float: "right" }}>
              <Button type="primary" onClick={() => history.goBack()}>
                Main
              </Button>

              <Button type="link" danger onClick={() => dispatch(doLogout())}>
                Logout
              </Button>
            </div>
          </Col>
          <Col span={8}>
            <UserDetailsComponent userInfo={userInfo} />
          </Col>
        </Row>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="transactionContainer">
          <h2>{Strings.appTexts.sendFundsPageTitle}</h2>
          <Form
            {...layout}
            name="transaction-form"
            className="transactionForm"
            initialValues={{ remember: true }}
            form={form}
            onFinish={onFinish}
          >
            <Form.Item
              label="User name"
              name="userNameField"
              rules={[{ required: true, message: "Please input user name!" }]}
            >
              <AutoComplete
                className="inputWidth"
                options={usersList}
                placeholder="Search user to send money"
                filterOption={filterUsers}
              />
            </Form.Item>

            <Form.Item
              label="Amount"
              name="amountField"
              rules={[{ required: true, message: "Please input amount!" }]}
            >
              <InputNumber
                className="inputWidth"
                formatter={(value: any) =>
                  `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value: any) => value.replace(/\$\s?|(,*)/g, "")}
              />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Row>
                <Col span={8}>
                  <Form.Item {...tailLayout}>
                    <Link to="/" className="link">
                      Cancel
                    </Link>
                  </Form.Item>
                </Col>
                <Col span={16}>
                  <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" className="submit">
                      Send
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
          </Form>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        {Strings.appTexts.copyright}
      </Footer>
    </Layout>
  );
};
