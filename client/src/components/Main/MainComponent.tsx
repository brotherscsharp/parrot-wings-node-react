import React, { useEffect } from "react";
import {
  doLogout,
  getUserInfo,
  getUserTransactions,
} from "../../reducers/ParrotWingsReducer";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Layout, Row } from "antd";
import { TransactionsComponent } from "../Transactions/TransactionsComponent";
import "./MainComponent.css";
import { useHistory } from "react-router-dom";
import { UserDetailsComponent } from "../UserDetails/UserDetailsComponent";
import Strings from "../../constants";

const { Header, Content, Footer } = Layout;

export const MainComponent = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const userInfo = useSelector((state: any) => state.pw.userInfo);
  const transactions = useSelector((state: any) => state.pw.transactions);

  useEffect(() => {
    // @ts-ignore
    dispatch(getUserInfo({}));
    // @ts-ignore
    dispatch(getUserTransactions({}));
  }, [dispatch, history, userInfo]);

  return (
    <div>
      <Layout className="layout">
        <Header>
          <p className="headerText">{Strings.appTexts.appName}</p>
          <Row>
            <Col span={16}>
              <div style={{ float: "right" }}>
                <Button
                  type="primary"
                  onClick={() => history.push("/sendmoney")}
                >
                  {Strings.appTexts.sendMoney}
                </Button>

                <Button type="link" danger onClick={() => dispatch(doLogout())}>
                  {Strings.appTexts.logout}
                </Button>
              </div>
            </Col>
            <Col span={8}>
              <UserDetailsComponent userInfo={userInfo} />
            </Col>
          </Row>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <div className="site-layout-content">
            <Row>
              <Col span={3}></Col>
              <Col span={18}>
                <h2>{Strings.appTexts.transactionsTitle}</h2>
                <TransactionsComponent transactions={transactions} />
              </Col>
              <Col span={3}></Col>
            </Row>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          {Strings.appTexts.copyright}
        </Footer>
      </Layout>
    </div>
  );
};
