import React from "react";
import { Button, Empty, List, Modal, Space, Tooltip } from "antd";
import {
  PoundOutlined,
  CalendarOutlined,
  DownloadOutlined,
  UploadOutlined,
  RetweetOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import Moment from "moment";
import "./TransactionsComponent.css";
import Strings from "../../constants";
import { ITransaction } from "../../protocols/ITransaction";

// @ts-ignore
const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

type Props = {
  transactions: ITransaction[];
};

export const TransactionsComponent = ({ transactions }: Props) => {
  const { confirm } = Modal;

  const repeatTransaction = (transaction: any) => {
    confirm({
      title: "Repeat transaction",
      icon: <ExclamationCircleOutlined />,
      content: `Are you sure you want send ${Math.abs(
        transaction.amount
      )}PW to ${transaction.username} `,
      onOk() {
        /*dispatch(
          sendFunds({
            id: transaction.id,
            amount: Math.abs(transaction.amount),
          })
        );*/
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  if (transactions && transactions.length > 0) {
    return (
      <List
        bordered
        dataSource={transactions}
        renderItem={(item: any, index: number) => (
          <List.Item
            key={index}
            actions={[
              <IconText
                icon={item.amount < 0 ? UploadOutlined : DownloadOutlined}
                text={item.amount}
                key="list-vertical-star-o"
              />,
              <IconText
                icon={PoundOutlined}
                text={item.balance}
                key="list-vertical-like-o"
              />,
              <IconText
                icon={CalendarOutlined}
                text={Moment(new Date(item.date)).format("D MMM YYYY")}
                key="list-vertical-message"
              />,

              item.amount < 0 && (
                <Tooltip title={Strings.appTexts.repeatTooltip}>
                  <Button onClick={() => repeatTransaction(item)}>
                    <IconText
                      icon={RetweetOutlined}
                      text={Strings.appTexts.repeat}
                      key="list-vertical-repeat"
                    />
                  </Button>
                </Tooltip>
              ),
            ]}
          >
            {item.amount < 0 ? (
              <div className="transactionMessage">
                {Strings.appTexts.outgoingTransaction}
                <b>{item.username}</b>
              </div>
            ) : (
              <div className="transactionMessage">
                {Strings.appTexts.incomingTransaction}
                <b>{item.userfrom}</b>
              </div>
            )}
          </List.Item>
        )}
      />
    );
  } else {
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
  }
};
