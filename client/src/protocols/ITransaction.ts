export interface ITransaction {
  id: number;
  date: Date;
  username: string;
  balance: number;
  amount: number;
}
