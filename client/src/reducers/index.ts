import { combineReducers } from "redux";
import ParrotWingsReducer from "./ParrotWingsReducer";
import SendFundsReducer from "./SendFundsReducer";

export default combineReducers({
  pw: ParrotWingsReducer,
  funds: SendFundsReducer,
});
