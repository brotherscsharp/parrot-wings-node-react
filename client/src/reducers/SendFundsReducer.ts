import {
  createSlice,
  createAsyncThunk,
  unwrapResult,
  PayloadAction,
} from "@reduxjs/toolkit";
import { ITransaction } from "../protocols/ITransaction";
import { IUser } from "../protocols/IUser";
import { getUserInfo, getUserTransactions } from "./ParrotWingsReducer";
import { UsersService } from "../networking/UsersService";
import { MoneyService } from "../networking/MoneyService";

export type ISendFunds = {
  transaction?: ITransaction;
  filteredUsers: IUser[];
  message: string;
};

export const INITIAL_STATE: ISendFunds = {
  filteredUsers: [],
  transaction: undefined,
  message: "",
};
export const getUsersList = createAsyncThunk(
  "sendFundsSlice/getAllUsers", // eslint-disable-next-line
  async ({}, { dispatch, rejectWithValue }) => {
    try {
      dispatch(backToInitialState());
      const res = await UsersService.getAllUsers();
      return { users: res.data as IUser[] };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const getUserByName = createAsyncThunk(
  "sendFundsSlice/getUserByName",
  async (payload: { name: string }, { dispatch, rejectWithValue }) => {
    try {
      dispatch(backToInitialState());
      const res = await UsersService.getUserByName(payload.name);
      return { users: res.data as IUser[] };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const sendFunds = createAsyncThunk(
  "sendFundsSlice/sendFunds",
  async (
    payload: { id: string; amount: number },
    { dispatch, rejectWithValue }
  ) => {
    try {
      dispatch(backToInitialState());
      const res = await MoneyService.sendFunds(payload.id, payload.amount);

      // @ts-ignore
      dispatch(getUserTransactions({}));

      // @ts-ignore
      dispatch(getUserInfo({}));
      return { users: res.data.trans_token as ITransaction };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const sendFundsSlice = createSlice({
  name: "sendFundsSlice",
  initialState: INITIAL_STATE,
  reducers: {
    backToInitialState: (state, action: PayloadAction) => {
      state.message = "";
    },
  },
  extraReducers: {
    // @ts-ignore
    [getUserByName.fulfilled]: (state, action) => {
      if (getUserByName.fulfilled.match(action)) {
        const user = unwrapResult(action);
        if (user.users) {
          state.filteredUsers = user.users;
        }
      } else {
        console.log(`Update failed: ${action.payload.error}`);
      }
    },
    // @ts-ignore
    [getUserByName.rejected]: (state, action) => {
      if (action.payload) {
        state.message = action.payload.message;
      } else {
        state.message = "Ooops! Something went wrong";
      }
    },
    // @ts-ignore
    [sendFunds.fulfilled]: (state, action) => {
      if (sendFunds.fulfilled.match(action)) {
        const result = unwrapResult(action);
        if (result) {
          state.message = "success";
        } else {
          state.message = "Ooops! Something went wrong";
        }
      }
    },
    // @ts-ignore
    [sendFunds.rejected]: (state, action) => {
      if (action.payload) {
        state.message = action.payload.message;
        // state.error = action.payload;
      } else {
        state.message = "Ooops! Something went wrong";
      }
    },
    // @ts-ignore
    [getUsersList.fulfilled]: (state, action) => {
      if (getUsersList.fulfilled.match(action)) {
        const user = unwrapResult(action);
        if (user) {
          state.filteredUsers = user.users || [];
        }
      } else {
        console.log(`Update failed: ${action.payload.error}`);
      }
    },
    // @ts-ignore
    [getUsersList.rejected]: (state, action) => {
      if (action.payload) {
        state.message = action.payload.message;
      } else {
        state.message = "Ooops! Something went wrong";
      }
    },
  },
});

export const { backToInitialState } = sendFundsSlice.actions;

export default sendFundsSlice.reducer;
