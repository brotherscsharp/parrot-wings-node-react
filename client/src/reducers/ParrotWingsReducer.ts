import { AuthService } from "../networking/AuthService";
import { ITransaction } from "../protocols/ITransaction";
import { IUser } from "../protocols/IUser";
import {
  createSlice,
  PayloadAction,
  createAsyncThunk,
  unwrapResult,
} from "@reduxjs/toolkit";
import { setAuthToken } from "../networking/api";
import { MoneyService } from "../networking/MoneyService";
import { UsersService } from "../networking/UsersService";
import { IUserInfo } from "../protocols/IUserInfo";

export type IParrotWings = {
  userInfo: IUserInfo;
  transactions: ITransaction[];
  error: string;
  token: string;
};

export const INITIAL_STATE: IParrotWings = {
  userInfo: {
    name: "",
    balance: 0,
    id: 0,
    email: "",
  },
  transactions: [],
  error: "",
  token: "",
};

export const doLogin = createAsyncThunk(
  "parrotWings/login",
  async (payload: { email: string; password: string }, { rejectWithValue }) => {
    try {
      const { email, password } = payload;
      const res = await AuthService.login(email, password);

      AuthService.saveUserData({
        token: res.data.id_token,
        email: email,
      });
      setAuthToken(res.data.id_token);
      return { email: email, token: res.data.id_token };
    } catch (error) {
      AuthService.clearUserData();
      return rejectWithValue(error.response.data);
    }
  }
);

export const doRegister = createAsyncThunk(
  "parrotWings/register",
  async (
    payload: { email: string; password: string; username: string },
    { rejectWithValue }
  ) => {
    try {
      const { email, password, username } = payload;
      const res = await AuthService.register(email, password, username);

      AuthService.saveUserData({
        token: res.data.id_token,
        email: email,
      });
      setAuthToken(res.data.id_token);

      return {
        username: username,
        email: email,
        token: res.data.id_token,
      };
    } catch (error) {
      AuthService.clearUserData();
      return rejectWithValue(error.response.data);
    }
  }
);

export const getUserInfo = createAsyncThunk(
  "parrotWings/getUserInfo", // eslint-disable-next-line
  async ({}, { rejectWithValue }) => {
    try {
      const res = await UsersService.getUserInfo();
      console.log(">>>>>> res = ", res);
      return {
        id: res.data.id,
        name: res.data.name,
        mail: res.data.email,
        balance: res.data.balance,
      };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const getUserTransactions = createAsyncThunk(
  "parrotWings/getUserTransactions", // eslint-disable-next-line
  async ({}, { rejectWithValue }) => {
    try {
      const res = await MoneyService.getUserTransactions();
      console.log(">>>>>> res = ", res);
      return { trans_token: res.data as ITransaction[] };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const getUserByName = createAsyncThunk(
  "parrotWings/getUserByName",
  async (payload: { name: string }, { rejectWithValue }) => {
    try {
      const res = await UsersService.getUserByName(payload.name);
      console.log(">>>>>> res = ", res);
      return { users: res.data as IUser[] };
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

const parrotWingsSlice = createSlice({
  name: "parrotWings",
  initialState: INITIAL_STATE,
  reducers: {
    doLogout: (state, action: PayloadAction) => {
      AuthService.clearUserData();
    },
    usersListRequest: (state, action: PayloadAction<string>) => {
      AuthService.clearUserData();
      state.token = "";
      state.error = action.payload;
    },
    backPwToInitialState: (state, action: PayloadAction) => {
      state.error = "";
    },
  },
  extraReducers: {
    // @ts-ignore
    [doLogin.fulfilled]: (state, action) => {
      if (doLogin.fulfilled.match(action)) {
        const user = unwrapResult(action);
        if (user) {
          state.token = action.payload.token;
          state.error = "";
        }
      } else {
        if (action.payload) {
          console.log(`Update failed: ${action.payload.error}`);
        } else {
          console.log(`Update failed: ${action.error}`);
        }
      }
    },
    // @ts-ignore
    [doLogin.rejected]: (state, action) => {
      state.token = "";
      if (action.payload) {
        state.error = action.payload.message;
        // state.error = action.payload;
      } else {
        state.error = "Ooops! Something went wrong";
      }
    },
    // @ts-ignore
    [doRegister.fulfilled]: (state, action) => {
      if (doRegister.fulfilled.match(action)) {
        const user = unwrapResult(action);
        console.log("<<<<<<< 2 >>>>>>", user);
        if (user) {
          // @ts-ignore
          state.userInfo.name = user.username;
          AuthService.saveUserData({
            email: user.email,
            token: user.token,
          });
        }
      }
    },
    // @ts-ignore
    [doRegister.rejected]: (state, action) => {
      state.token = "";
      if (action.payload) {
        state.error = action.payload.message;
        // state.error = action.payload;
      } else {
        state.error = "Ooops! Something went wrong";
      }
    },
    // @ts-ignore
    [getUserInfo.fulfilled]: (state, action) => {
      if (getUserInfo.fulfilled.match(action)) {
        const user = unwrapResult(action);
        console.log("<<<<<<< 3 >>>>>>", user);
        if (user) {
          state.userInfo.id = user.id;
          state.userInfo.name = user.name;
          state.userInfo.email = user.mail;
          state.userInfo.balance = user.balance;
          AuthService.saveUserData({
            email: user.mail,
            token: undefined,
          });
        }
      }
    },
    // @ts-ignore
    [getUserInfo.rejected]: (state, action) => {
      if (action.payload) {
        state.error = action.payload.message;
        // state.error = action.payload;
      } else {
        state.error = "Ooops! Something went wrong";
      }
    },
    // @ts-ignore
    [getUserTransactions.fulfilled]: (state, action) => {
      if (getUserTransactions.fulfilled.match(action)) {
        const user = unwrapResult(action);
        console.log("<<<<<<< 4 >>>>>>", user);
        if (user) {
          // @ts-ignore
          state.transactions = user.trans_token?.sort((one, two) =>
            one > two ? 1 : -1
          );
        }
      }
    },
    // @ts-ignore
    [getUserTransactions.rejected]: (state, action) => {
      if (action.payload) {
        state.error = action.payload.message;
        // state.error = action.payload;
      } else {
        state.error = "Ooops! Something went wrong";
      }
    },
  },
});

export const {
  doLogout,
  usersListRequest,
  backPwToInitialState,
} = parrotWingsSlice.actions;

export default parrotWingsSlice.reducer;
