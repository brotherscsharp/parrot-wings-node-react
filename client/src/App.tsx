import React from "react";
import "./App.css";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import reducers from "./reducers";
import NavigationComponent from "./components/Navigation/NavigationComponent";
import "antd/dist/antd.css";

const store = configureStore({
  reducer: reducers,
});
export type AppDispatch = typeof store.dispatch;

function App() {
  return (
    <Provider store={store}>
      <NavigationComponent />
    </Provider>
  );
}

export default App;
