export default {
  currencySign: "PW",

  appTexts: {
    appName: "Parrot Wings",
    copyright: "Servin Osmanov ©2020",
    sendFundsPageTitle: "Send funds to your friend",
    logInTitle: "Sing in to continue",
    registerTitle: "Sing up to continue",
    transactionsTitle: "Transactions",
    sendMoney: "Send money",
    logout: "Logout",
    balance: "Balance",
    repeat: "Repeat",
    repeatTooltip: "you can send funds to this user again",
    outgoingTransaction: "Outgoing transaction: to ",
    incomingTransaction: "Incoming transaction: from ",
  },

  messages: {
    transactionSuccess: "Transaction success",
    userNotFound: "Such user does not exists",
  },

  endpoints: {
    serverBaseUrl: "http://localhost:3100",
    register: "/auth/register",
    login: "/auth/login",
    userTransactions: "/transactions",
    sendFunds: "/transactions",
    currentUserInfo: "/users/info",
    filterUsers: "/users",
    allUsers: "/users",

    /*
    serverBaseUrl: "http://193.124.114.46:3001",
    register: "/users",
    login: "/sessions/create",
    userTransactions: "/api/protected/transactions",
    sendFunds: "/api/protected/transactions",
    currentUserInfo: "/api/protected/user-info",
    filterUsers: "/api/protected/users/list",
    allUsers: "/api/protected/users",*/
  },
};
