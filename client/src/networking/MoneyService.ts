import Strings from "../constants";
import api from "./api";

export class MoneyService {
  static getUserTransactions = async () =>
    await api.get(Strings.endpoints.userTransactions);

  static sendFunds = async (id: string, amount: number) =>
    await api.post(Strings.endpoints.sendFunds, {
      userid: id,
      amount,
    });
}
