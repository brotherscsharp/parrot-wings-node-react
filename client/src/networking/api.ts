import axios from "axios";
import Strings from "../constants";

const api = axios.create({
  baseURL: Strings.endpoints.serverBaseUrl,
  timeout: 5000,
  headers: {
    "Content-Type": "application/json",
  },
});

export const setAuthToken = (token: string) => {
  if (token) {
    //applying token
    api.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  }
};

export default api;
