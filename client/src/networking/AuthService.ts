import Strings from "../constants";
import { Cookies } from "react-cookie";
import api from "./api";

const cookie = new Cookies();
export class AuthService {
  static login = async (email: string, password: string) => {
    return await api.post(
      `${Strings.endpoints.serverBaseUrl}${Strings.endpoints.login}`,
      { email, password }
    );
  };

  static register = async (
    email: string,
    password: string,
    username: string
  ) => {
    return await api.post(
      `${Strings.endpoints.serverBaseUrl}${Strings.endpoints.register}`,
      { email, password, username }
    );
  };

  static isUserLoggedIn = (): boolean => {
    const token = cookie.get("token");
    return token ? true : false;
  };

  // @ts-ignore
  static saveUserData = ({ email, token }) => {
    let cookieOptions = { maxAge: 36000 };
    if (token) cookie.set("token", token, cookieOptions);
    if (email) cookie.set("email", email, cookieOptions);
  };

  static clearUserData = () => {
    cookie.remove("token");
    cookie.remove("email");
  };
}
