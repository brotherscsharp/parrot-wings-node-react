import Strings from "../constants";
import api from "./api";

export class UsersService {
  static getUserInfo = async () =>
    await api.get(Strings.endpoints.currentUserInfo);

  static getUserByName = async (name: string) =>
    await api.post(Strings.endpoints.filterUsers, { filter: name });

  static getAllUsers = async () => await api.get(Strings.endpoints.allUsers);
}
